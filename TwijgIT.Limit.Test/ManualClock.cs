namespace TwijgIT.Limit.Test {
  using System;

  using Microsoft.Extensions.Internal;

  public class ManualClock : ISystemClock {
    public ManualClock() : this(DateTimeOffset.UtcNow) { }

    private ManualClock(DateTimeOffset utcNow) {
      this.UtcNow = utcNow;
    }

    public DateTimeOffset UtcNow { get; private set; }

    public void Forward(TimeSpan timeSpan) {
      this.UtcNow += timeSpan;
    }
  }
}
