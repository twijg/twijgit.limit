namespace TwijgIT.Limit.Test {
  using Xunit;

  using static System.TimeSpan;

  public class LeakyBucketTests {
    [Fact]
    public void LeakyBucketEvaporate() {
      var leakyBucket = new LeakyBucket(2, FromSeconds(4), new ManualClock());
      Assert.True(leakyBucket.TestAndDrop());
      Assert.True(leakyBucket.TestAndDrop());
      Assert.False(leakyBucket.TestAndDrop());
      leakyBucket.Evaporate();
      Assert.True(leakyBucket.TestAndDrop());
      Assert.False(leakyBucket.TestAndDrop());
    }

    [Fact]
    public void LeakyBucketFill() {
      var systemClock = new ManualClock();
      var leakyBucket = new LeakyBucket(2, FromSeconds(4), systemClock);
      Assert.True(leakyBucket.TestAndDrop());
      Assert.True(leakyBucket.TestAndDrop());
      Assert.False(leakyBucket.TestAndDrop());
      Assert.False(leakyBucket.TestAndDrop());
      Assert.False(leakyBucket.TestAndDrop());
      systemClock.Forward(FromSeconds(1));
      Assert.False(leakyBucket.TestAndDrop());
      systemClock.Forward(FromSeconds(1));
      Assert.True(leakyBucket.TestAndDrop());
      Assert.False(leakyBucket.TestAndDrop());
    }
  }
}
