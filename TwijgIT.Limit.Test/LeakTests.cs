namespace TwijgIT.Limit.Test {
  using Xunit;

  using static System.TimeSpan;

  public class LeakTests {
    [Fact]
    public void LeakAll() {
      Assert.Equal(0, LeakyBucket.Leak(MaxValue, FromMinutes(1), 1.5, 2));
    }

    [Fact]
    public void LeakHalf() {
      Assert.Equal(0.5, LeakyBucket.Leak(FromMinutes(1), FromMinutes(2), 1.5, 2));
    }

    [Fact]
    public void LeakNothing() {
      Assert.Equal(1.5, LeakyBucket.Leak(Zero, FromMinutes(1), 1.5, 2));
    }

    [Fact]
    public void LeakUnderflow() {
      Assert.Equal(0, LeakyBucket.Leak(FromMinutes(1), FromMinutes(2), 0.5, 2));
    }
  }
}
