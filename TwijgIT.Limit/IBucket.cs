namespace TwijgIT.Limit {
  /// <summary>
  /// A bucket that can be dropped into and evaporated from.
  /// </summary>
  public interface IBucket {
    /// <summary>
    /// Tries to add a drop to the bucket.
    /// </summary>
    /// <returns>Whether drop could be added without overflow.</returns>
    bool TestAndDrop();

    /// <summary>
    /// Evaporates a drop from the bucket.
    /// </summary>
    void Evaporate();
  }
}
