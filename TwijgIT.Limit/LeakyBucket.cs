namespace TwijgIT.Limit {
  using System;

  using JetBrains.Annotations;

  using Microsoft.Extensions.Internal;
  using Microsoft.Extensions.Options;

  /// <summary>
  /// Implements leaky bucket.
  /// </summary>
  [PublicAPI]
  public class LeakyBucket : IBucket {
    private readonly TimeSpan leakTimeSpan;
    private readonly object Lock = new object();
    private readonly double size;
    private readonly ISystemClock systemClock;
    private double content;
    private DateTimeOffset latestDrop = DateTimeOffset.MinValue;

    /// <summary>
    /// Initializes a new instance of the LeakyBucket class.
    /// </summary>
    /// <param name="size">The maximum number of drops in the bucket.</param>
    /// <param name="leakTimeSpan">The time needed to leak all drops.</param>
    /// <param name="systemClock">The system clock.</param>
    /// <exception cref="ArgumentOutOfRangeException">Any param is zero or negative.</exception>
    /// <exception cref="ArgumentNullException">Any param is null.</exception>
    public LeakyBucket(double size, TimeSpan leakTimeSpan, [NotNull]ISystemClock systemClock) {
      if (!(leakTimeSpan.TotalMilliseconds > 0)) {
        throw new ArgumentOutOfRangeException(nameof(leakTimeSpan), "Leak time span must be positive.");
      }

      if (!(size > 0)) {
        throw new ArgumentOutOfRangeException(nameof(size), "Size must be positive.");
      }

      this.leakTimeSpan = leakTimeSpan;
      this.systemClock = systemClock ?? throw new ArgumentNullException(nameof(systemClock));
      this.size = size;
      this.content = 0;
    }

    /// <summary>
    /// Initializes a new instance of the LeakyBucket class.
    /// </summary>
    /// <param name="options">The options.</param>
    /// <param name="systemClock">The system clock.</param>
    /// <exception cref="ArgumentOutOfRangeException">Any option is zero or negative.</exception>
    /// <exception cref="ArgumentNullException">Param systemClock is null.</exception>
    public LeakyBucket([CanBeNull]IOptions<LeakyBucketOptions> options, [NotNull]ISystemClock systemClock) : this(
      options?.Value?.Size ?? LeakyBucketOptions.DefaultSize,
      options?.Value?.LeakTimeSpan ?? LeakyBucketOptions.DefaultLeakTimeSpan,
      systemClock) { }

    /// <summary>
    /// Tries to add a drop to the bucket.
    /// </summary>
    /// <returns>Whether drop could be added without overflow.</returns>
    public bool TestAndDrop() {
      lock (this.Lock) {
        var now = this.systemClock.UtcNow;
        var next = Leak(now - this.latestDrop, this.leakTimeSpan, this.content, this.size) + 1;
        if (next > this.size) {
          return false;
        }

        this.latestDrop = now;
        this.content = next;
        return true;
      }
    }

    /// <summary>
    /// Evaporates a drop from the bucket.
    /// </summary>
    public void Evaporate() {
      lock (this.Lock) {
        this.content = Math.Max(this.content - 1, 0);
      }
    }

    /// <summary>
    /// Determines content level after leaking.
    /// </summary>
    /// <param name="duration">The timespan during which is leaked.</param>
    /// <param name="leakTimeSpan">The timespan to empty a full bucket.</param>
    /// <param name="content">The content before leaking.</param>
    /// <param name="size">The bucket size.</param>
    /// <returns>The content after leaking.</returns>
    [Pure]
    public static double Leak(TimeSpan duration, TimeSpan leakTimeSpan, double content, double size) {
      return content > 0 && duration < leakTimeSpan
               ? Math.Max(content - size * duration.Ticks / leakTimeSpan.Ticks, 0)
               : 0;
    }
  }
}
