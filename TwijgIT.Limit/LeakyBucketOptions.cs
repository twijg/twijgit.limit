namespace TwijgIT.Limit {
  using System;

  using JetBrains.Annotations;

  /// <summary>
  /// Options for the <see cref="LeakyBucket"/> class.
  /// </summary>
  [PublicAPI]
  public class LeakyBucketOptions {
    /// <summary>
    /// The default bucket size.
    /// </summary>
    public const int DefaultSize = 25;
    
    /// <summary>
    /// The default time to leak all drops.
    /// </summary>
    public static readonly TimeSpan DefaultLeakTimeSpan = TimeSpan.FromSeconds(10);

    /// <summary>
    /// Gets or sets the bucket size.
    /// </summary>
    public int Size { get; set; } = DefaultSize;
    
    /// <summary>
    /// Gets or sets the time to leak all drops.
    /// </summary>
    public TimeSpan LeakTimeSpan { get; set; } = DefaultLeakTimeSpan;
  }
}
